import { shallowMount } from "@vue/test-utils";
import Counter from "@/components/Counter.vue";

describe("test counter", () => {
  test("test if we can increment by 1 as default step", async () => {
    const wrapper = shallowMount(Counter);
    const btnIncrment = wrapper.find(".counter__more");
    const value = wrapper.find(".counter__value");

    await btnIncrment.trigger("click");
    expect(Number(value.element.value)).toEqual(1);
  });
  test("test if we can increment 3 times by 1 as default step ", async () => {
    const wrapper = shallowMount(Counter);
    const btnIncrment = wrapper.find(".counter__more");
    const value = wrapper.find(".counter__value");
    for (let i = 0; i < 3; i++) {
      await btnIncrment.trigger("click");
    }
    expect(Number(value.element.value)).toEqual(3);
  });
  test("test if we can decrement by 2 step as default 3 times ", async () => {
    const wrapper = shallowMount(Counter);
    const btnDecrement = wrapper.find(".counter__less");
    await wrapper.setProps({ step: 2 });
    const value = wrapper.find(".counter__value");
    for (let i = 0; i < 3; i++) {
      await btnDecrement.trigger("click");
    }
    expect(Number(value.element.value)).toEqual(-6);
  });
  test("test if we can increment by 1 step, step 3 time ", async () => {
    const wrapper = shallowMount(Counter);
    await wrapper.setProps({ step: 3 });
    const btnIncrment = wrapper.find(".counter__more");
    const value = wrapper.find(".counter__value");
    for (let i = 0; i < 3; i++) {
      await btnIncrment.trigger("click");
    }
    expect(Number(value.element.value)).toEqual(9);
  });
});
