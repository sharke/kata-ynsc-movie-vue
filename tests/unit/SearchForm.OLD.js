import { shallowMount, createLocalVue } from "@vue/test-utils";
import SearchForm from "@/components/SearchForm.vue";
import Vuex from "vuex";
import VueI18n from "vue-i18n";
import trad from "@/locale/trad";
import movies from "@/store/modules/movies.js";

describe("SearchForm.vue testing : [ search form testting ]", () => {
  let store;
  let wrapper;
  let i18n;
  const localVue = createLocalVue();
  beforeEach(async () => {
    localVue.use(Vuex);
    localVue.use(VueI18n);
    i18n = new VueI18n({
      locale: "en", // set default locale
      messages: trad, // set locale messages
    });
    store = new Vuex.Store({
      modules: {
        movies: movies,
        namespaced: true,
      },
    });
    wrapper = shallowMount(SearchForm, {
      store,
      localVue,
      propsData: {},
      data: {},
      i18n,
    });
    await wrapper.vm.fetchMovies();
  });

  test("test if the fetchMovie() work, and nbre of all movies == 600", async () => {
    const nbrAllMoviesStore = movies.state.movies.length;
    let ValueExpectation = 600;
    expect(nbrAllMoviesStore).toEqual(ValueExpectation);
  });

  test("test setKeywordTitle(keyword) work", async () => {
    await wrapper.vm.setKeywordTitle("bois");
    let ValueExpectation = "bois";
    expect(movies.state.keywordTitle).toEqual(ValueExpectation);
  });

  test("test when we put 'bois' as keyword search we have 3 result", async () => {
    await wrapper.vm.setKeywordTitle("bois");
    let ValueExpectation = 3;
    expect(movies.state.filtredMovies.length).toEqual(ValueExpectation);
  });

  test("test when we put 'Xz2!8qZ4' as inexisting keyword search we have 0 result", async () => {
    await wrapper.vm.setKeywordTitle("Xz2!8qZ4");
    let ValueExpectation = 0;
    expect(movies.state.filtredMovies.length).toEqual(ValueExpectation);
  });

  afterEach(async () => {
    // cleaning
    movies.state.movies = [];
    movies.state.filtredMovies = [];
    movies.state.keywordTitle = null;
    movies.state.currentPage = 1;
    movies.state.currentMovie = null;
  });
});
