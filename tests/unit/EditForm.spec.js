// TDOD : ( WIP )
import { shallowMount, createLocalVue } from "@vue/test-utils";
import EditForm from "@/components/EditForm.vue";
import Vuex from "vuex";
import movies from "@/store/modules/movies.js";
describe("EditForm.vue testing : [ editing ... ]", () => {
  let store;
  let wrapper;
  let firstMovie;
  const localVue = createLocalVue();
  beforeEach(async () => {
    localVue.use(Vuex);
    store = new Vuex.Store({
      modules: {
        movies: movies,
        namespaced: true,
      },
    });
    // TODO pb test to resolve : the data currentMovie is always null in EditForm.vue in test mode !!!
    /* wrapper = shallowMount(EditForm, { store, localVue });
    (firstMovie = {
      id: 1,
      endYear: 1986,
      genres: "Documentary,Short",
      isAdult: false,
      originalTitle: "Carmencita",
      primaryTitle: "Carmencita",
      runtimeMinute: "2",
      startYear: 1904,
      tConst: "tt0000001",
      dateChargement: "2021-10-19T16:09:14.446Z",
      titleType: "short",
    }),
      await wrapper.setData({
        currentMovie: firstMovie,
      }); */
  });

  test("test update Minute for movie id = 1 ", async () => {
    // mount cpt

    // const inputMinute = wrapper.find("#minute");

    // search input title
    // tester si la modification ce passe
    expect(1).toEqual(1);
  });

  afterEach(async () => {
    // cleaning
    movies.state.movies = [];
    movies.state.filtredMovies = [];
    movies.state.keywordTitle = null;
    movies.state.currentPage = 1;
    movies.state.currentMovie = null;
  });
});
