import { shallowMount, createLocalVue } from "@vue/test-utils";
import MovieCard from "@/components/MovieCard.vue";
import Vuex from "vuex";
import VueI18n from "vue-i18n";
import trad from "@/locale/trad";
import movies from "@/store/modules/movies.js";

describe("MovieCard.vue testing :", () => {
  let store;
  let wrapper;
  let oneMovie;
  let i18n;
  const localVue = createLocalVue();
  beforeEach(async () => {
    localVue.use(Vuex);
    localVue.use(VueI18n);
    i18n = new VueI18n({
      locale: "en", // set default locale
      messages: trad, // set locale messages
    });
    store = new Vuex.Store({
      modules: {
        movies: movies,
        namespaced: true,
      },
    });
    wrapper = shallowMount(MovieCard, {
      store,
      localVue,
      propsData: {},
      data: {},
      i18n,
    });
    (oneMovie = {
      id: 6,
      endYear: "\\N",
      genres: "Short",
      isAdult: false,
      originalTitle: "Chinese Opium Den",
      primaryTitle: "Chinese Opium Den",
      runtimeMinute: "1",
      startYear: "1894",
      tConst: "tt0000006",
      dateChargement: "2021-10-19T16:09:14.461Z",
      titleType: "short",
    }),
      await wrapper.setProps({
        movie: oneMovie,
      });
    // await wrapper.vm.fetchMovies();
  });

  test("test that movieCard display that right info", async () => {
    let titleCard = wrapper.find(".movie-card__primary-title");
    expect(titleCard.text()).toEqual(oneMovie.primaryTitle);
  });

  afterEach(async () => {
    // cleaning
    movies.state.movies = [];
    movies.state.filtredMovies = [];
    movies.state.keywordTitle = null;
    movies.state.currentPage = 1;
    movies.state.currentMovie = null;
  });
  /* test("test if CPT verfiay ValueExpectation as 50 ", async () => {
      // mount cpt
      const wrapper = shallowMount(CPT);
  
      // find a marquer in CPT
      const w = wrapper.find(".classMarguer");
  
      //  expecting and testing
      let ValueExpectation = "50";
      expect(w.html()).toContain(ValueExpectation);
    }); */
  /* test("test if a nbr result > 0 when we search a existin word", async () => {
      const wrapper = shallowMount(SearchForm);
      await wrapper.setData({ keywords: "bois" });
      const w = wrapper.find(".search-form__nbr-result");
      expect(w.html()).toContain(4);
    }); */
});
