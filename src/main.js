import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import { BootstrapVue, BootstrapVueIcons } from "bootstrap-vue";
import "bootstrap-vue/dist/bootstrap-vue.css";
import VueI18n from "vue-i18n";
import trad from "./locale/trad";

// install BootstrapVue
Vue.use(BootstrapVue);
Vue.use(BootstrapVueIcons); // doc https://icons.getbootstrap.com/icons/list/
// install vue-i18n
Vue.use(VueI18n);

const i18n = new VueI18n({
  locale: "en", // set default locale
  messages: trad, // set locale messages
});

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  i18n,
  render: (h) => h(App),
}).$mount("#app");
