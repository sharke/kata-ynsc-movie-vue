export default {
  // english
  en: {
    common: {
      editing: "Editing",
      yes: "Yes",
      no: "No",
      newMovie: " New movie",
      creation: "Create a new movie",
      downloadData: "Download DATA",
      nbrResult: "Number of results :",
      alertDeletingMovie:
        "Attention, vous voulez vraiment supprimer ce film ? : ",
    },
    movie: {
      startYear: "Start year",
      endYear: "End year",
      isAdult: "Is adult",
    },
    Search: {
      searchMovie: "Search a movie",
    },
  },
  // frensh
  fr: {
    common: {
      editing: "Edition",
      yes: "Oui",
      no: "Non",
      newMovie: " Nouveau film",
      creation: "creer un nouveau film !",
      downloadData: "charger desdonnées",
      nbrResult: "Nombre de resultat :",
      alertDeletingMovie:
        "Attention, vous voulez vraiment supprimer ce film ? : ",
    },
    movie: {
      startYear: "Annee de début",
      endYear: "Annee de fin",
      isAdult: "Pour adult",
    },
    Search: {
      searchMovie: "Rechercher un film",
    },
  },
};
