import axios from "axios";
import { db, isFirebaseSource } from "@/api/firebase-config.js";
import {
  collection,
  getDocs,
  updateDoc,
  doc,
  deleteDoc,
  addDoc,
} from "firebase/firestore";
// globals
export const API_MOVIES = "http://localhost:5000/movies/";
// npx json-server --watch ./src/assets/mocks/movie-data.json --port 5000
//export const API_MOVIES =
("https://kata-ynsc-movie-default-rtdb.europe-west1.firebasedatabase.app/movies");

const moviesCollectionRef = collection(db, "movies"); // from firebase !!

// for bypassing error of unused variable !!
export const NBR_MOVIE_BY_PAGE = 9;

// Getters
export const GET_MOVIES = "getMovies";
export const GET_MOVIES_BY_PAGE = "getMoviesByPage";
export const GET_CURRENT_MOVIE = "getCurrentMovie";
export const GET_CURRENT_PAGE = "getCurrentPage";

// Actions
export const FETCH_MOVIES = "fetchMovies";
export const SET_CURRENT_PAGE = "setCurrentPage";
export const SET_CURRENT_MOVIE = "setCurrentMovie";
export const SET_KEYWORD_TITLE = "setKeywordTitle";
export const UPDATE_MOVIE = "updateMovie";
export const DELETE_MOVIE = "deleteMovie";
export const ADD_MOVIE = "addMovie";

// Mutations
export const SET_MOVIES = "setMovies";

// cleaner

export const state = {
  // all movies from API
  movies: [],
  // filtred movies by page or by keyword title
  filtredMovies: [],
  // a current movie selected ( editing ...)
  currentMovie: null,
  // current page select
  currentPage: 1,
  // a keywordTitle taped by user
  keywordTitle: null,
};
const getters = {
  [GET_MOVIES]: (state) => {
    state.filtredMovies = state.movies;
    // filter by keyword title search
    if (state.keywordTitle !== null && state.keywordTitle !== "") {
      state.filtredMovies = state.filtredMovies.filter((movie) =>
        movie.primaryTitle
          .toLowerCase()
          .includes(state.keywordTitle.toLowerCase())
      );
    }
    return state.filtredMovies;
  },
  [GET_CURRENT_MOVIE]: (state) => {
    return state.currentMovie;
  },
  [GET_MOVIES_BY_PAGE]: (state) => {
    // make liste by pages
    return state.filtredMovies.slice(
      (state.currentPage - 1) * NBR_MOVIE_BY_PAGE,
      state.currentPage * NBR_MOVIE_BY_PAGE
    );
  },
  [GET_CURRENT_PAGE]: (state) => {
    return state.currentPage;
  },
};
const actions = {
  async [FETCH_MOVIES]({ commit }) {
    try {
      if (isFirebaseSource) {
        const AllMovies = await getDocs(moviesCollectionRef);

        let allMoviesTmp = [];
        AllMovies.docs.map((doc) => {
          // car ds firebase les id sont ds un  niveau superieur que data , en va reecrire la liste envotée au state du store :
          allMoviesTmp = [...allMoviesTmp, { ...doc.data(), id: doc.id }];
        });
        commit(SET_MOVIES, allMoviesTmp); // OLD : AllMovies.data
      } else {
        // if a local mock data
        const AllMovies = await axios.get(API_MOVIES);
        commit(SET_MOVIES, AllMovies.data);
      }
    } catch (e) {
      console.error("error fetch FETCH_MOVIES");
      console.error(e);

      throw e;
    }
  },
  [SET_CURRENT_PAGE]({ commit }, pageNbr) {
    commit(SET_CURRENT_PAGE, pageNbr);
  },
  [SET_CURRENT_MOVIE]({ commit }, currentMovie) {
    commit(SET_CURRENT_MOVIE, currentMovie);
  },
  [SET_KEYWORD_TITLE]({ commit }, keywordTitle) {
    commit(SET_KEYWORD_TITLE, keywordTitle);
  },
  async [DELETE_MOVIE](NULL, deletedMovie) {
    // commit(DELETE_MOVIE, currentMovie);
    // to fix prittier error
    // console.log("commit ", commit);
    if (isFirebaseSource) {
      const movieDoc = doc(db, "movies", deletedMovie.id);
      await deleteDoc(movieDoc);
    } else {
      await axios.delete(API_MOVIES + deletedMovie.id);
    }
  },
  async [UPDATE_MOVIE](NULL, updatedMovie) {
    try {
      if (isFirebaseSource) {
        // en selectionne le doc a modifier ds firebse
        const movieDoc = doc(db, "movies", updatedMovie.id);
        await updateDoc(movieDoc, updatedMovie); // maj on firebase
        // commit(UPDATE_MOVIE, updatedMovie);
      } else {
        await axios.put(API_MOVIES + "/" + updatedMovie.id, updatedMovie);
      }
    } catch (e) {
      console.error("action/UPDATE_MOVIE");
      console.error(e);

      throw e;
    }
  },
  async [ADD_MOVIE]({ commit }, newMovie) {
    try {
      if (isFirebaseSource) {
        await addDoc(moviesCollectionRef, newMovie);
        commit(ADD_MOVIE, newMovie);
      } else {
        await axios.post(API_MOVIES, newMovie);
      }
    } catch (e) {
      console.error("action/ADD_MOVIE");
      console.error(e);
      throw e;
    }
  },
};
const mutations = {
  [SET_MOVIES]: (state, payload) => {
    state.movies = payload;
  },
  [SET_CURRENT_PAGE](state, payload) {
    state.currentPage = payload;
  },
  [SET_CURRENT_MOVIE](state, payload) {
    state.currentMovie = payload;
  },
  [SET_KEYWORD_TITLE](state, payload) {
    state.keywordTitle = payload;
  },
  [UPDATE_MOVIE](state, payload) {
    state.currentMovie = payload;
  },
  [ADD_MOVIE](state, payload) {
    state.movies.push(payload);
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
