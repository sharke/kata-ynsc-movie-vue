// ce fichier doit pas etre versionnée
export const myFirebaseConfig = {
  apiKey: "YourPrivatKay",
  authDomain: "authDomain.firebaseapp.com",
  databaseURL: "databaseURL",
  projectId: "projectId",
  storageBucket: "storageBucket.appspot.com",
  messagingSenderId: "YourPrivateMessagingSenderId",
  appId: "YourPrivatAppID",
  enable: false, // when we define a valid connextion info firebase we put it at true , by default is false
};
